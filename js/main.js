(function(){

  var configure = function() {
    require.config({
      baseUrl: 'js',
      paths: {
        models: 'models',
        views: 'views',
        routers: 'routers',
        backbone: 'lib/backbone-min',
        underscore: 'lib/underscore-min',
        jquery: 'lib/jquery-1.10.2.min'
      },
      shim: {
        backbone: {
          deps: ['underscore','jquery'],
          exports: 'Backbone'
        },
        underscore: {
          exports: '_'
        }
      }
    });
  };

  var boot = function() {
    configure();
  };

  boot();

})();

//step 1. build up router, router hits initial view of our todo list view
//step 2. set up view to render "Hello World." (domain div, _ templating etc)
//step 3. Set up simple prototype of UI for Todo items
//step 4. Set up Model with attributes
//step 5. Hook up Backbone local storage
//step 6. Finish up the booting process


